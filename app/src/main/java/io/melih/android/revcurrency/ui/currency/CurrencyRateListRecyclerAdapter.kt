/*
 * Copyright 2019 RevCurrency
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.melih.android.revcurrency.ui.currency

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import io.melih.android.revcurrency.R

class CurrencyRateListRecyclerAdapter(
    private val amountListener: (String) -> Unit,
    private val itemClickListener: (CurrencyItemViewEntity) -> Unit
) : ListAdapter<CurrencyItemViewEntity, CurrencyRateViewHolder>(CURRENCY_DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyRateViewHolder =
        CurrencyRateViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_currency_rate,
                parent,
                false
            ),
            amountListener,
            itemClickListener
        )

    override fun onBindViewHolder(
        viewHolder: CurrencyRateViewHolder,
        position: Int,
        payloads: MutableList<Any>
    ) {
        if (payloads.isEmpty()) {
            super.onBindViewHolder(viewHolder, position, payloads)
            return
        }
        val bundle = payloads[0] as Bundle
        val currencyValue = bundle.getString(KEY_CURRENCY_VALUE) ?: "100"
        viewHolder.bindTo(position, currencyValue)
    }

    override fun onBindViewHolder(viewHolder: CurrencyRateViewHolder, position: Int) {
        viewHolder.bindTo(position, getItem(position))
    }
}

private const val KEY_CURRENCY_VALUE = "KEY_CURRENCY_VALUE"
val CURRENCY_DIFF_CALLBACK = object : DiffUtil.ItemCallback<CurrencyItemViewEntity>() {
    override fun areItemsTheSame(
        oldItem: CurrencyItemViewEntity,
        newItem: CurrencyItemViewEntity
    ): Boolean =
        oldItem.currencyCode == newItem.currencyCode

    override fun areContentsTheSame(
        oldItem: CurrencyItemViewEntity,
        newItem: CurrencyItemViewEntity
    ): Boolean =
        oldItem == newItem

    override fun getChangePayload(
        oldItem: CurrencyItemViewEntity,
        newItem: CurrencyItemViewEntity
    ): Any? {
        val diffBundle = Bundle()
        if (newItem.currencyValue !== oldItem.currencyValue) {
            diffBundle.putString(KEY_CURRENCY_VALUE, newItem.currencyValue)
        }

        return if (diffBundle.size() == 0) null else diffBundle
    }
}
