/*
 * Copyright 2019 RevCurrency
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.melih.android.revcurrency

import io.melih.android.revcurrency.model.Currency
import io.melih.android.revcurrency.ui.currency.CurrencyItemViewEntity
import java.math.BigDecimal

/**
 * Test data for unit tests.
 */
object TestData {
    val CURRENCY_ITEM_VIEW_ENTITY_LIST: List<CurrencyItemViewEntity> = mutableListOf(
        CurrencyItemViewEntity(R.drawable.ic_european_union, "EUR", "Euro", "1"),
        CurrencyItemViewEntity(R.drawable.ic_australia, "AUD", "Australian Dollar", "1.6133"),
        CurrencyItemViewEntity(R.drawable.ic_bulgaria, "BGN", "Bulgarian Lev", "1.9521"),
        CurrencyItemViewEntity(R.drawable.ic_brazil, "BRL", "Brazilian Real", "4.7827"),
        CurrencyItemViewEntity(R.drawable.ic_canada, "CAD", "Canadian Dollar", "1.5309")
    )

//    private val testDispatcher= TestCoroutineDispatcher()

    val CURRENCY_LIST: List<Currency> = mutableListOf(
        Currency("EUR", BigDecimal.valueOf(1)),
        Currency("AUD", BigDecimal.valueOf(1.6133)),
        Currency("BGN", BigDecimal.valueOf(1.9521)),
        Currency("BRL", BigDecimal.valueOf(4.7827)),
        Currency("CAD", BigDecimal.valueOf(1.5309))
    )
}
